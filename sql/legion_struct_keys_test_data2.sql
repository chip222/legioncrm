-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 11 2015 г., 11:27
-- Версия сервера: 5.5.46
-- Версия PHP: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `legion`
--

-- --------------------------------------------------------

--
-- Структура таблицы `billets`
--

CREATE TABLE `billets` (
  `ID` int(11) NOT NULL,
  `BILLETNAME` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `billets`
--

INSERT INTO `billets` (`ID`, `BILLETNAME`) VALUES
(0, 'Разработчик');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `ID` int(11) NOT NULL,
  `CRDATE` datetime DEFAULT NULL,
  `CLNAME` tinytext,
  `CLOCATION` tinytext,
  `PHONENUM` tinytext,
  `SOURCEID` int(11) DEFAULT NULL,
  `MANAGERID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`ID`, `CRDATE`, `CLNAME`, `CLOCATION`, `PHONENUM`, `SOURCEID`, `MANAGERID`) VALUES
(0, '2015-12-11 13:09:13', 'Вася Пупкин', 'Троещина. ул. Драйзера', '(044) 515-3413', 0, 1),
(1, '2015-12-09 17:12:35', 'Дуня Кулачкова', 'Гостомель', '04453441618', 1, 0),
(2, '2015-12-11 12:14:15', 'Толик Шустрый', 'Киев Частный дом 2', '04453442613', 0, 0),
(3, '2015-12-10 18:27:09', 'Семен Лобанов', 'Россия', '542353453453', 2, 1),
(4, '2015-12-10 18:25:41', 'Вася Кролик', 'Где-то далеко', '2312323424234', 1, 0),
(5, '2015-12-11 12:14:33', 'Гарик Харламов', 'Москва', '3453453453345335', 0, 1),
(6, '2015-12-11 09:38:39', 'Алина Кабаева', 'Быковня', '23453453453453', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `clientsavor`
--

CREATE TABLE `clientsavor` (
  `ID` int(11) NOT NULL,
  `CLIENTID` int(11) NOT NULL,
  `SERVICEID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `department`
--

CREATE TABLE `department` (
  `ID` int(11) NOT NULL,
  `DEPNAME` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `department`
--

INSERT INTO `department` (`ID`, `DEPNAME`) VALUES
(0, 'ИТ отдел');

-- --------------------------------------------------------

--
-- Структура таблицы `managers`
--

CREATE TABLE `managers` (
  `ID` int(11) NOT NULL,
  `CRDATE` datetime NOT NULL,
  `FULLNAME` tinytext NOT NULL,
  `LOGIN` tinytext NOT NULL,
  `PASSWORD` tinytext NOT NULL,
  `LASTLOGIN` datetime DEFAULT NULL,
  `DEPID` int(11) NOT NULL,
  `BILLETID` int(11) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `managers`
--

INSERT INTO `managers` (`ID`, `CRDATE`, `FULLNAME`, `LOGIN`, `PASSWORD`, `LASTLOGIN`, `DEPID`, `BILLETID`, `ACTIVE`) VALUES
(0, '2015-12-08 06:22:00', 'Евгений Нечипорук', 'chip', '31572fac52c2e7063ffed287ba7d6fb8', '2015-12-08 00:00:00', 0, 0, 1),
(1, '2015-12-09 17:21:39', 'Administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, 0, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE `services` (
  `ID` int(11) NOT NULL,
  `SNAME` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`ID`, `SNAME`) VALUES
(0, 'Охрана квартиры'),
(1, 'Охрана дома'),
(2, 'Коммерческая недвижимость'),
(3, 'Физ. охрана');

-- --------------------------------------------------------

--
-- Структура таблицы `sourceinfo`
--

CREATE TABLE `sourceinfo` (
  `ID` int(11) NOT NULL,
  `SOURCENAME` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sourceinfo`
--

INSERT INTO `sourceinfo` (`ID`, `SOURCENAME`) VALUES
(0, 'Заявка'),
(1, 'Интернет'),
(2, 'Деж телефон');

-- --------------------------------------------------------

--
-- Структура таблицы `stage`
--

CREATE TABLE `stage` (
  `ID` int(11) NOT NULL,
  `STAGENAME` tinytext NOT NULL,
  `STAGETYPE` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `stage`
--

INSERT INTO `stage` (`ID`, `STAGENAME`, `STAGETYPE`) VALUES
(0, 'Переговоры', 1),
(1, 'Договор', 1),
(2, 'Отказ', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `treaty`
--

CREATE TABLE `treaty` (
  `ID` int(11) NOT NULL,
  `CRDATE` datetime NOT NULL,
  `CLIENTID` int(11) NOT NULL,
  `MANAGERID` int(11) NOT NULL,
  `STAGEID` int(11) NOT NULL,
  `COMMENT` text NOT NULL,
  `NEXTCONTACT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `treaty`
--

INSERT INTO `treaty` (`ID`, `CRDATE`, `CLIENTID`, `MANAGERID`, `STAGEID`, `COMMENT`, `NEXTCONTACT`) VALUES
(0, '2015-12-08 00:00:00', 0, 0, 0, 'Любезно пообщались с клиентом', '2015-12-13');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `billets`
--
ALTER TABLE `billets`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SOURCEID` (`SOURCEID`),
  ADD KEY `MANAGERID` (`MANAGERID`);

--
-- Индексы таблицы `clientsavor`
--
ALTER TABLE `clientsavor`
  ADD KEY `CLIENTID` (`CLIENTID`),
  ADD KEY `SERVICEID` (`SERVICEID`);

--
-- Индексы таблицы `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `managers`
--
ALTER TABLE `managers`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `BILLETID` (`BILLETID`),
  ADD KEY `DEPID` (`DEPID`);

--
-- Индексы таблицы `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `sourceinfo`
--
ALTER TABLE `sourceinfo`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `stage`
--
ALTER TABLE `stage`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `treaty`
--
ALTER TABLE `treaty`
  ADD KEY `CLIENTID` (`CLIENTID`),
  ADD KEY `MANAGERID` (`MANAGERID`),
  ADD KEY `STAGEID` (`STAGEID`);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`SOURCEID`) REFERENCES `sourceinfo` (`ID`),
  ADD CONSTRAINT `clients_ibfk_2` FOREIGN KEY (`MANAGERID`) REFERENCES `managers` (`ID`);

--
-- Ограничения внешнего ключа таблицы `clientsavor`
--
ALTER TABLE `clientsavor`
  ADD CONSTRAINT `clientsavor_ibfk_1` FOREIGN KEY (`SERVICEID`) REFERENCES `services` (`ID`),
  ADD CONSTRAINT `clientsavor_ibfk_2` FOREIGN KEY (`CLIENTID`) REFERENCES `clients` (`ID`);

--
-- Ограничения внешнего ключа таблицы `managers`
--
ALTER TABLE `managers`
  ADD CONSTRAINT `managers_ibfk_1` FOREIGN KEY (`BILLETID`) REFERENCES `billets` (`ID`),
  ADD CONSTRAINT `managers_ibfk_2` FOREIGN KEY (`DEPID`) REFERENCES `department` (`ID`);

--
-- Ограничения внешнего ключа таблицы `treaty`
--
ALTER TABLE `treaty`
  ADD CONSTRAINT `treaty_ibfk_1` FOREIGN KEY (`MANAGERID`) REFERENCES `managers` (`ID`),
  ADD CONSTRAINT `treaty_ibfk_2` FOREIGN KEY (`STAGEID`) REFERENCES `stage` (`ID`),
  ADD CONSTRAINT `treaty_ibfk_3` FOREIGN KEY (`CLIENTID`) REFERENCES `clients` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
