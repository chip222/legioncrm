<?php

class HtmlOut
{	
	public static function table($data, $params)
	{
		$retResult = '<table ' . $params .'>';
		foreach ($data as $oneRow) {
			$retResult .= '<tr>';
			foreach ($oneRow as $rowItem) {
				$retResult .= '<td>' . $rowItem . '</td>';
			}
			$retResult .= '</tr>';
		}
		$retResult .= '</table>';
		return $retResult;
	}
	
	public static function link($name, $url)
	{
		return '<a href="' . $url . '">' . $name . '</a>';
	}
	//<h2 onmouseover="tooltip(this,'Это просто пример всплывающей<br /> подсказки JavaScript!')" onmouseout="hide_info(this)">
	public static function evLink($name, $url, $comment) {
		return "<a href=\"" . $url . "\" onmouseover=\"tooltip(this,'" . $comment . "')\" onmouseout=\"hide_info(this)\">" . $name . "</a>";		
	}
}

?>