-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 17 2015 г., 15:26
-- Версия сервера: 5.5.46
-- Версия PHP: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `legion`
--

-- --------------------------------------------------------

--
-- Структура таблицы `billets`
--

CREATE TABLE `billets` (
  `ID` int(11) NOT NULL,
  `BILLETNAME` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `billets`
--

INSERT INTO `billets` (`ID`, `BILLETNAME`) VALUES
(0, 'Разработчик'),
(1, 'Администратор');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `ID` int(11) NOT NULL,
  `CRDATE` datetime DEFAULT NULL,
  `CLNAME` tinytext,
  `CLOCATION` tinytext,
  `PHONENUM` tinytext,
  `SOURCEID` int(11) DEFAULT NULL,
  `MANAGERID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`ID`, `CRDATE`, `CLNAME`, `CLOCATION`, `PHONENUM`, `SOURCEID`, `MANAGERID`) VALUES
(0, '2015-12-11 13:09:13', 'Вася Пупкин', 'Троещина. ул. Драйзера', '(044) 515-3413', 0, 1),
(1, '2015-12-09 17:12:35', 'Дуня Кулачкова', 'Гостомель', '04453441618', 1, 0),
(2, '2015-12-11 12:14:15', 'Толик Шустрый', 'Киев Частный дом 2', '04453442613', 0, 0),
(3, '2015-12-10 18:27:09', 'Семен Лобанов', 'Россия', '542353453453', 2, 1),
(4, '2015-12-10 18:25:41', 'Вася Кролик', 'Где-то далеко', '2312323424234', 1, 0),
(5, '2015-12-11 12:14:33', 'Гарик Харламов', 'Москва', '3453453453345335', 0, 1),
(6, '2015-12-11 09:38:39', 'Алина Кабаева', 'Быковня', '23453453453453', 1, 0),
(7, '2015-12-17 10:37:28', 'Елена Цыганкова', 'ул. Соломенская, 16б', '(093) 45698711', 1, 1),
(8, '2015-12-17 10:41:32', 'Александр Искра', 'ул. Соломенская, 14', '2750578', 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `clientsavor`
--

CREATE TABLE `clientsavor` (
  `ID` int(11) DEFAULT NULL,
  `CLIENTID` int(11) DEFAULT NULL,
  `SERVICEID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `clientsavor`
--

INSERT INTO `clientsavor` (`ID`, `CLIENTID`, `SERVICEID`) VALUES
(7, 2, 1),
(8, 2, 2),
(9, 2, 3),
(10, 0, 1),
(12, 1, 0),
(13, 3, 0),
(14, 3, 3),
(15, 6, 0),
(16, 6, 2),
(18, 7, 0),
(19, 8, 0),
(20, 8, 3),
(21, 5, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `department`
--

CREATE TABLE `department` (
  `ID` int(11) NOT NULL,
  `DEPNAME` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `department`
--

INSERT INTO `department` (`ID`, `DEPNAME`) VALUES
(0, 'ИТ отдел');

-- --------------------------------------------------------

--
-- Структура таблицы `managers`
--

CREATE TABLE `managers` (
  `ID` int(11) NOT NULL,
  `CRDATE` datetime NOT NULL,
  `FULLNAME` tinytext NOT NULL,
  `LOGIN` tinytext NOT NULL,
  `PASSWORD` tinytext NOT NULL,
  `LASTLOGIN` datetime DEFAULT NULL,
  `DEPID` int(11) NOT NULL,
  `BILLETID` int(11) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `managers`
--

INSERT INTO `managers` (`ID`, `CRDATE`, `FULLNAME`, `LOGIN`, `PASSWORD`, `LASTLOGIN`, `DEPID`, `BILLETID`, `ACTIVE`) VALUES
(0, '2015-12-08 06:22:00', 'Евгений Нечипорук', 'chip', '31572fac52c2e7063ffed287ba7d6fb8', '2015-12-08 00:00:00', 0, 0, 1),
(1, '2015-12-09 17:21:39', 'Administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE `services` (
  `ID` int(11) NOT NULL,
  `SNAME` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`ID`, `SNAME`) VALUES
(0, 'Охрана квартиры'),
(1, 'Охрана дома'),
(2, 'Коммерческая недвижимость'),
(3, 'Физ. охрана');

-- --------------------------------------------------------

--
-- Структура таблицы `sourceinfo`
--

CREATE TABLE `sourceinfo` (
  `ID` int(11) NOT NULL,
  `SOURCENAME` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sourceinfo`
--

INSERT INTO `sourceinfo` (`ID`, `SOURCENAME`) VALUES
(0, 'Заявка'),
(1, 'Интернет'),
(2, 'Деж телефон');

-- --------------------------------------------------------

--
-- Структура таблицы `stage`
--

CREATE TABLE `stage` (
  `ID` int(11) NOT NULL,
  `STAGENAME` tinytext NOT NULL,
  `STAGETYPE` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `stage`
--

INSERT INTO `stage` (`ID`, `STAGENAME`, `STAGETYPE`) VALUES
(0, 'Переговоры', 1),
(1, 'Договор', 1),
(2, 'Отказ', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `treaty`
--

CREATE TABLE `treaty` (
  `ID` int(11) NOT NULL,
  `CRDATE` datetime DEFAULT NULL,
  `CLIENTID` int(11) DEFAULT NULL,
  `MANAGERID` int(11) DEFAULT NULL,
  `STAGEID` int(11) DEFAULT NULL,
  `COMMENT` text,
  `NEXTCONTACT` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `treaty`
--

INSERT INTO `treaty` (`ID`, `CRDATE`, `CLIENTID`, `MANAGERID`, `STAGEID`, `COMMENT`, `NEXTCONTACT`) VALUES
(0, '2015-12-17 09:41:46', 3, 1, 2, 'Проверочка коментария', '2015-12-31'),
(1, '2015-12-17 10:32:17', 3, 1, 1, 'ываываываыыв\r\nа\r\nыва\r\nыва\r\n\r\nыва', '2015-05-12'),
(2, '2015-12-17 10:33:42', 2, 1, 1, 'Договор\r\n', '2016-01-12'),
(3, '2015-12-17 10:34:26', 2, 1, 0, 'Переговоры', '2016-01-13'),
(4, '2015-12-17 10:42:43', 8, 1, 0, 'Клиент невменяемый, контакт перенесен.', '2015-12-20'),
(5, '2015-12-17 10:46:18', 8, 1, 1, 'Заключаем договор завтра', '2015-12-16'),
(6, '2015-12-17 11:49:23', 8, 1, 1, 'Все-таки заключаем договор\r\nЭто следующая строка\r\n', '2015-12-15'),
(7, '2015-12-17 11:54:13', 8, 1, 2, 'Клиент отказался от всего вообще', '2015-12-17'),
(8, '2015-12-17 11:59:55', 0, 1, 0, 'Начинаем переговоры.\r\nКлиент согласен на что-то\r\n', '2015-12-15'),
(9, '2015-12-17 12:05:40', 8, 1, 0, 'вфывфыв', '2015-12-15'),
(10, '2015-12-17 12:06:38', 8, 1, 2, 'трулу лу', '2015-12-16'),
(11, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '2015-12-17 12:37:22', 7, 1, 0, 'квепнкенкененк', '2015-12-23'),
(13, '2015-12-17 12:38:27', 7, 1, 0, 'зщшщзшш', '2030-12-24'),
(14, '2015-12-17 12:38:36', 7, 1, 0, '', '2015-12-10'),
(15, '2015-12-17 12:38:45', 7, 1, 0, '', '2015-12-31'),
(16, '2015-12-17 12:39:01', 7, 1, 2, '', '2015-12-21'),
(17, '2015-12-17 12:39:19', 7, 1, 1, 'ывапвпвап', '2015-12-01'),
(18, '2015-12-17 12:40:26', 4, 1, 0, 'Трам парам парам', '2015-12-18'),
(19, '2015-12-17 12:52:25', 4, 1, 0, 'Вася, такой вася.<br><br>Аэто вторая строка<br><br>А это третья строка.<br><br>', '2015-12-18'),
(20, '2015-12-17 12:53:25', 4, 1, 0, 'А теперь еще раз\r<br>2\r<br>3\r<br>4\r<br>5', '2015-12-20'),
(21, '2015-12-17 13:04:16', 4, 1, 0, 'Многострочный коментарий\r<br>А это вторая строка\r<br>А это уже тертья строка\r<br>КУ ку\r<br>', '2015-12-15'),
(22, '2015-12-17 13:37:30', 5, 1, 1, 'Проверочка\r<br>Проверочка2\r<br>', '2015-12-09'),
(23, '2015-12-17 14:16:40', 2, 1, 1, 'Завтра.\r<br>Все будет завтра.\r<br>', '2015-12-18'),
(24, '2015-12-17 14:29:42', 5, 1, 2, 'Глупый клиент отказался.\r<br>И был не прав.', '2015-12-19'),
(25, '2015-12-17 15:29:36', 8, 1, 2, '111', '2015-12-01'),
(26, '2015-12-17 15:30:35', 8, 1, 1, '222', '2015-12-02'),
(27, '2015-12-17 15:39:13', 2, 1, 2, '555', '2015-12-18'),
(28, '2015-12-17 15:41:31', 8, 1, 0, '54555', '2015-12-14'),
(29, '2015-12-17 15:43:34', 1, 1, 0, '111', '2015-12-23'),
(30, '2015-12-17 15:43:51', 6, 1, 0, '111', '2015-12-25'),
(31, '2015-12-17 15:44:41', 0, 1, 0, '', '2015-12-18'),
(32, '2015-12-17 15:44:53', 0, 1, 1, '', '2015-12-19'),
(33, '2015-12-17 15:57:16', 8, 1, 2, '232323', '2015-12-30'),
(34, '2015-12-17 17:22:27', 5, 1, 0, 'А переговоры все идут и идут', '2015-12-20');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `billets`
--
ALTER TABLE `billets`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SOURCEID` (`SOURCEID`),
  ADD KEY `MANAGERID` (`MANAGERID`);

--
-- Индексы таблицы `clientsavor`
--
ALTER TABLE `clientsavor`
  ADD KEY `CLIENTID` (`CLIENTID`),
  ADD KEY `SERVICEID` (`SERVICEID`);

--
-- Индексы таблицы `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `managers`
--
ALTER TABLE `managers`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `BILLETID` (`BILLETID`),
  ADD KEY `DEPID` (`DEPID`);

--
-- Индексы таблицы `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `sourceinfo`
--
ALTER TABLE `sourceinfo`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `stage`
--
ALTER TABLE `stage`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `treaty`
--
ALTER TABLE `treaty`
  ADD UNIQUE KEY `ID` (`ID`),
  ADD KEY `CLIENTID` (`CLIENTID`),
  ADD KEY `MANAGERID` (`MANAGERID`),
  ADD KEY `STAGEID` (`STAGEID`);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`SOURCEID`) REFERENCES `sourceinfo` (`ID`),
  ADD CONSTRAINT `clients_ibfk_2` FOREIGN KEY (`MANAGERID`) REFERENCES `managers` (`ID`);

--
-- Ограничения внешнего ключа таблицы `clientsavor`
--
ALTER TABLE `clientsavor`
  ADD CONSTRAINT `clientsavor_ibfk_1` FOREIGN KEY (`SERVICEID`) REFERENCES `services` (`ID`),
  ADD CONSTRAINT `clientsavor_ibfk_2` FOREIGN KEY (`CLIENTID`) REFERENCES `clients` (`ID`);

--
-- Ограничения внешнего ключа таблицы `managers`
--
ALTER TABLE `managers`
  ADD CONSTRAINT `managers_ibfk_1` FOREIGN KEY (`BILLETID`) REFERENCES `billets` (`ID`),
  ADD CONSTRAINT `managers_ibfk_2` FOREIGN KEY (`DEPID`) REFERENCES `department` (`ID`);

--
-- Ограничения внешнего ключа таблицы `treaty`
--
ALTER TABLE `treaty`
  ADD CONSTRAINT `treaty_ibfk_1` FOREIGN KEY (`MANAGERID`) REFERENCES `managers` (`ID`),
  ADD CONSTRAINT `treaty_ibfk_2` FOREIGN KEY (`STAGEID`) REFERENCES `stage` (`ID`),
  ADD CONSTRAINT `treaty_ibfk_3` FOREIGN KEY (`CLIENTID`) REFERENCES `clients` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
