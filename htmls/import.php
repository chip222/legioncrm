<?php
$uploaddir = getcwd() . '/uploads/';
$uploadfile = $uploaddir . basename($_FILES['csvuserfile']['name']);

if (!move_uploaded_file($_FILES['csvuserfile']['tmp_name'], $uploadfile)) {
	die('Возможная атака с помощью файловой загрузки!');
}

/*
	[0]. Дата создания записи
	[1]. Время создания записи
	[2]. Имя клиента
	[3]. Телефон клиента
	[4]. Название услуги для клиента
	[5]. Источник информации (откуда узнал)
	[6]. Месторасположение клиента
	[7]. Закрепленный менеджер
	[8]. Номер отдела продаж
	[9]. Коментарий к переговорам
	[10]. Дата след. контакта
	[11]. Статус переговоров
	[12]. В случае отказа - причина отказа
 */

if (!isset($sqlWork)) { $sqlWork = new SQLWork(); }

if (($handle = fopen($uploadfile, "r")) !== FALSE) {
	while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
		if ((isset($data[3])) && (strlen($data[3]) > 0)) {
			echo $sqlWork->convertDateTimeToMySql($data[0], $data[1]) . "<br />\n";
			echo iconv('Windows-1251', 'UTF-8', trim($data[2])) . "<br />\n";
			echo trim($data[3]) . "<br />\n";
			echo iconv('Windows-1251', 'UTF-8', trim($data[4])) . "<br />\n";
			echo iconv('Windows-1251', 'UTF-8', trim($data[5])) . "<br />\n";
			echo iconv('Windows-1251', 'UTF-8', trim($data[6])) . "<br />\n";
			echo iconv('Windows-1251', 'UTF-8', trim($data[7])) . "<br />\n";
			echo trim($data[8]) . "<br />\n";
			echo iconv('Windows-1251', 'UTF-8', trim($data[9])) . "<br />\n";
			echo $sqlWork->convertDateToMySql(trim($data[10])) . "<br />\n";
			echo iconv('Windows-1251', 'UTF-8', trim($data[11])) . "<br />\n";
			echo iconv('Windows-1251', 'UTF-8', trim($data[12])) . "<br />\n";
			echo '<hr>';
			$sqlWork->importClientRawData($sqlWork->convertDateTimeToMySql($data[0], $data[1]), 
					iconv('Windows-1251', 'UTF-8', trim($data[2])), 
					trim($data[3]), 
					iconv('Windows-1251', 'UTF-8', trim($data[4])), 
					iconv('Windows-1251', 'UTF-8', trim($data[5])), 
					iconv('Windows-1251', 'UTF-8', trim($data[6])), 
					iconv('Windows-1251', 'UTF-8', trim($data[7])), 
					trim($data[8]), 
					iconv('Windows-1251', 'UTF-8', trim($data[9])), 
					$sqlWork->convertDateToMySql(trim($data[10])), 
					iconv('Windows-1251', 'UTF-8', trim($data[11])), 
					iconv('Windows-1251', 'UTF-8', trim($data[12])));
		}
	}
	fclose($handle);
}
?>
