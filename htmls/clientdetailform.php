<?php
if (!isset($sqlWork)) { $sqlWork = new SQLWork(); }
$ci = $sqlWork->getClientInfo($_GET['clientid']);
?>
<center>
<fieldset>
<legend>Общая</legend>
<form method="post" action="index.php?action=updateclient">
<input type="hidden" name="clientid" value="<?php echo $ci['ID']; ?>">
<table>
<tr>
<td align="right">Дата:</td><td><input type="text" size="20" maxlength="20" value="<?php echo $ci['CRDATE']; ?>" READONLY></td>
</tr>
<tr>
<td align="right">Имя клиента:</td><td><input name="cname" type="text" size="50" maxlength="50" value="<?php echo $ci['CLNAME']; ?>"></td>
</tr>
<tr>
<td align="right">Месторасположение:</td><td><input name="clocation" type="text" size="50" maxlength="50" value="<?php echo $ci['CLOCATION']; ?>"></td>
</tr>
<tr>
<td align="right">Номер телефона:</td><td><input name="cphonenum" type="text" size="50" maxlength="50" value="<?php echo $ci['PHONENUM']; ?>"></td>
</tr>
<tr>
<td align="right">Откуда узнал:</td>
<td>
<select name="csourceid" size="1">
<?php
$slist = $sqlWork->getSourcesList();
$selectedword = '';
foreach ($slist as $id => $sr) {
	if ($id == $ci['SOURCEID']) { $selectedword = 'selected'; }
	echo '<option ' . $selectedword . ' value="' . $id . '">' . $sr . '</option>';
	$selectedword = '';
}
?>
</select>
</td>
</tr>
<tr>
<td align="right">Закрепленный менеджер:</td>
<td>
<select name="cmanagerid" size="1">
<?php
$mlist = $sqlWork->getManagerList();
$selectedword = '';
foreach ($mlist as $id => $mn) {
	if ($id == $ci['MANAGERID']) { $selectedword = 'selected'; }
	echo '<option ' . $selectedword . ' value="' . $id . '">' . $mn . '</option>';
	$selectedword = '';
}
?>
</select>
</td>
</tr>
<tr>
<td></td>
<td><input type="submit" value="Сохранить"></td>
</tr>
</table>
</form>
</fieldset>
<fieldset>
<legend>Интересующие услуги</legend>
<form method="post" action="index.php?action=updateclientserv">
<input type="hidden" name="clientid" value="<?php echo $ci['ID']; ?>">
<?php
$servList = $sqlWork->getServiceList();
$clServList = $sqlWork->getClientServicesList($ci['ID']);
$checkedword = '';
foreach ($servList as $id => $serv) {
	foreach ($clServList as $clServ) {
		if ($clServ == $id) {
			$checkedword = ' checked ';
		}
	}
	echo '<input type="checkbox" name="serv' . $id . '"' . $checkedword . '>' . $serv;
	$checkedword = '';
}
?>
<br><input type="submit" value="Сохранить">
</form>
</fieldset>
<fieldset>
<legend>Переговоры</legend>
<form method="post" action="index.php?action=addclienttreaty">
<input type="hidden" name="clientid" value="<?php echo $ci['ID']; ?>">
<table>
<tr><td>
Стадия: 
<select name="stageid" size="1">
<?php
$stageList = $sqlWork->getStageList();
foreach ($stageList as $stageId => $stageName) {
	echo '<option value="' . $stageId . '">' . $stageName . '</option>'; 
}
?>
</select>
След. контакт:<input value="<?php echo date('d-m-Y') ?>" name="nextdate" readonly type="text" size="20" maxlength="20" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)">
</td></tr>
<tr><td>
<textarea name="comment" rows="10" cols="45" name="text"></textarea>
</td></tr>
<tr><td>
<input type="submit" value="Добавить">
</td></tr>
<tr><td>

</td></tr>
</table>
</form>
</fieldset>
<fieldset>
<legend>История переговоров</legend>
<table width="100%" border="1" id="treatlist">
<tr><td align="center"><b>Дата</b></td><td align="center"><b>Статус</b></td><td align="center"><b>Коментарий</b></td><td align="center"><b>След. контакт</b></td></tr>
<?php
$treatyList = $sqlWork->getClientTreaty($ci['ID']);
foreach ($treatyList as $treaty) {
	echo '<tr>';
	echo '<td>' . $treaty['CRDATE'] . '<br>(' . $treaty['FULLNAME'] . ')</td>';
	echo '<td>' . $treaty['STAGENAME'] . '</td>';
	echo '<td>' . $treaty['COMMENT'] . '</td>';
	echo '<td>' . $treaty['NEXTCONTACT'] . '</td>';
	echo '</tr>';
}
?>
</table>
</fieldset>
</center>
