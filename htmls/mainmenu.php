<?php
if (!isset($sqlWork)) { $sqlWork = new SQLWork(); }
$mi = $sqlWork->getManagerInfo($_SESSION['user_id']);
$csc = $sqlWork->getClientStatusesCount($_SESSION['user_id']);
?>
<div>
<table>
<tr>
<td><a href="index.php" title="На главную"><img alt="На главную" src="images/home.png" width="48" height="48"></a></td>
<td><a href="?action=addclient" title="Добавить клиента"><img alt="Добавить клиента" src="images/add.png" width="48" height="48"></a></td>
<td><a href="?action=import" title="Импортировать клиентов"><img alt="Импортировать клиентов" src="images/import.png" width="48" height="48"></a></td>
<td><a href="?logout" title="Выход"><img alt="Выход" src="images/applicationexit.png" width="48" height="48"></a></td>
<td>
<fieldset>
<legend>Сотрудник</legend>
<b><?php echo $mi['FULLNAME']; ?></b> (<?php echo $mi['BILLETNAME']; ?>)
<br>Отдел: <?php echo $mi['DEPNAME']; ?>
</fieldset>
</td>
<td>
<fieldset>
<legend>По времени</legend>
На сегодня: <?php echo $csc['TODAY']; ?>
<br>Просроченные: <?php echo $csc['OVERDUE']; ?>
</fieldset>
</td>
<td>
<fieldset>
<legend>По статусу</legend>
<?php
$sl = $sqlWork->getStageList();
foreach ($sl as $sid => $sname) {
	echo $sname . ': ' . $csc[$sid] . ' ';
}
?> 
</fieldset>
</td>
</tr>
</table>
</div>
<hr>
