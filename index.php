<?php
include_once 'includes/SQLWork.php';
include_once 'includes/HtmlOut.php';

//session_start();

/*
if (!isset($_SESSION['SQLWork'])) {
	echo 'Created';
	$_SESSION['SQLWork'] = new SQLWork();
}
*/
$sqlWork = new SQLWork();

if (isset($_GET['logout']))
{
	if (isset($_SESSION['user_id']))
	{
		unset($_SESSION['user_id']);
		setcookie('login', '', 0, "/");
		setcookie('password', '', 0, "/");
		session_destroy();
		header('Location: index.php');
		exit;
	}
}

if (isset($_POST['login']) && isset($_POST['password'])) {
	if ($sqlWork->validateUser($_POST['login'], $_POST['password']))
	{
		//session_start();
		$_SESSION['user_id'] = $sqlWork->getManagerId($_POST['login']);
	}
	else {
		echo ('В доступе отказанно');
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<script src="htmls/calendar_ru.js" type="text/javascript"></script>
<script src="htmls/hint.js" type="text/javascript"></script>
<title>Legion CRM</title>
</head>
<body>
<div id="mess" style="visibility: hidden;position: absolute;background: #fc0;padding: 5px;border: solid 1px black;float: left;"></div>
<!-- 
<h2 onmouseover="tooltip(this,'А это мой личный<br /> многострочный коментарий')" onmouseout="hide_info(this)">
It is just JavaScript Tooltip example!</h2>
 -->
<?php
if (!isset($_SESSION['user_id'])) {
	include_once 'htmls/authform.html';	
	exit;
}
if (!isset($_GET['action'])) {
	$action = 'main';
} else {
	$action = $_GET['action'];
}

switch ($action) {
	case 'main':
		include_once 'htmls/mainmenu.php';
		$needSort = false;
		if (isset($_GET['sortby'])) {
			if ($_GET['sortby'] == 'nextcontact') {
				$needSort = true;
			}
		}
		$clientsData = $sqlWork->getMainClientList($needSort);
		include_once 'htmls/mainform.php';		
		break;
	case 'addclient':
		include_once 'htmls/mainmenu.php';
		include_once 'htmls/addclientform.php';
		break;
	case 'saveclient':
		$newClientId = $sqlWork->getNewIdFromTable('clients');
		$sqlWork->saveClientInfo(
			$newClientId, 
			$_GET['cname'], 
			$_GET['clocation'], 
			$_GET['cphonenum'], 
			$_GET['csourceid'], 
			//$sqlWork->getManagerId($_SESSION['user_id'])
			$_SESSION['user_id']
		);
		//header('Location: index.php');
		header('Location: index.php?action=clientdetail&clientid=' . $newClientId);
		break;
	case 'clientdetail':
		include_once 'htmls/mainmenu.php';
		include_once 'htmls/clientdetailform.php';
		break;
	case 'updateclient':
		$sqlWork->saveClientInfo(
				$_POST['clientid'], 
				$_POST['cname'], 
				$_POST['clocation'], 
				$_POST['cphonenum'], 
				$_POST['csourceid'], 
				$_POST['cmanagerid']
		);
		header('Location: index.php?action=clientdetail&clientid=' . $_POST['clientid']);
		break;
	case 'updateclientserv':
		$sqlWork->updateClientServices($_POST);
		header('Location: index.php?action=clientdetail&clientid=' . $_POST['clientid']);
		break;
	case 'addclienttreaty':
		//$htmlComment = preg_replace('~[\\n\\r]+?~', '<br>', $_POST['comment']);
		$htmlComment = preg_replace('~[\\n]+?~', '<br />', $_POST['comment']);
		$htmlComment = preg_replace('~[\\r]+?~', '', $htmlComment);
		//$htmlComment = $_POST['comment'];
		$sqlWork->addTreaty(
			$_POST['clientid'], 
			//$sqlWork->getManagerId($_SESSION['user_id']),
			$_SESSION['user_id'],
			$_POST['stageid'], 
			$htmlComment,
			$sqlWork->dateToMySlq($_POST['nextdate'])
		);
		header('Location: index.php?action=clientdetail&clientid=' . $_POST['clientid']. '#treatlist');
		break;
	case 'import':
		include_once 'htmls/mainmenu.php';
		include_once 'htmls/upload.php';
		break;
	case 'uploadcsvfile':
		include_once 'htmls/mainmenu.php';
		if ($_FILES['csvuserfile']['error'] === UPLOAD_ERR_OK) {
			include_once 'htmls/import.php';
		} else {
			include_once 'htmls/upload.php';
			echo '<br>Ошибка загрузки файла на сервер: ' . $_FILES['csvuserfile']['error'] . '<br>';
		}
		break;
}
?>
</body>
</html>
