<?php

class SQLWork
{
	const dbhost = 'localhost';
	const dbuser = 'legion';
	const dbpass = 'legion';
	const dbname = 'legion';
	
	private  $dblink;
	
	public function __construct()
	{
		$this->dblink = new mysqli(
				SQLWork::dbhost,
				SQLWork::dbuser,
				SQLWork::dbpass,
				SQLWork::dbname);
		
		if(mysqli_connect_errno()){
			echo mysqli_connect_error();
		}
	}
	
	private function checkSQLError($query) {
		if ($this->dblink->error != '') {
			echo "SQL Error: " . $this->dblink->error;
			echo "<br><br>" . $query;
			die();
		}		
	}
	
	private function parseDateTime($inDate) {
		$dAr = date_parse($inDate);
		return $dAr['day'] . '-' . $dAr['month'] . '-' . $dAr['year'] . ' ' . $dAr['hour'] . ':' . $dAr['minute'];
	}
	
	private function parseDate($inDate) {
		$dAr = date_parse($inDate);
		return $dAr['day'] . '-' . $dAr['month'] . '-' . $dAr['year'];
	}
	
	public function convertDateToMySql($inDate) {
		$retResult = '';
		$darray = explode('.', $inDate);
		$day = $darray[0];
		$month = $darray[1];
		$year = $darray[2];
		$retResult = $year . '-' . $month . '-' . $day;
		return $retResult;
	}
	
	public function convertDateTimeToMySql($inDate, $inTime) {
		$retResult = '';
		$retResult = $this->convertDateToMySql($inDate) . ' ' . $inTime . ':00'; 
		return $retResult;
	}
	
	public function validateUser($username, $password)
	{
		$retResult = false;
		$sql = "SELECT * FROM `managers` WHERE `LOGIN` = '" . $username . "'";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		$row = $qresult->fetch_object();
		if ($row) {
			if (md5($password) == $row->PASSWORD) {
				$retResult = true;
			}
		}
		$qresult->close();
		return $retResult;
	}
	
	/*
	 SELECT * FROM `clients` AS `cl` LEFT JOIN `sourceinfo` AS `sr` ON `cl`.`SOURCEID` = `sr`.`ID` LEFT JOIN `managers` AS `mn` ON `cl`.`MANAGERID` = `mn`.`ID`
	 */
	
	private function getLastStageName($clientId) {
		$retResult = '';
		$sql = "SELECT `stage`.`STAGENAME` FROM `stage` WHERE `stage`.`ID` IN "
				. "(SELECT `treaty`.`STAGEID` FROM `treaty` WHERE `ID` IN (SELECT MAX(`treaty`.`ID`) FROM `treaty` WHERE `CLIENTID` = '" . $clientId . "'))";
		/*
		$sql = "SELECT MAX(`tr`.`ID`) AS `LASTID`, `st`.`STAGENAME` "
			. "FROM `treaty` AS `tr` "
			. "LEFT JOIN `stage` AS `st` ON `tr`.`STAGEID` = `st`.`STAGENAME` "
			. "WHERE `tr`.`CLIENTID` = '" . $clientId . "'";
		*/
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		$row = $qresult->fetch_object();
		if ($row) {
			$retResult = $row->STAGENAME;
		}
		return $retResult;
	}
	
	private function getLastStageId($clientId) {
		$retResult = '-1';
		$sql = "SELECT `treaty`.`STAGEID` FROM `treaty` WHERE `ID` IN (SELECT MAX(`treaty`.`ID`) FROM `treaty` WHERE `CLIENTID` = '" . $clientId . "')";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		$row = $qresult->fetch_object();
		if ($row) {
			$retResult = $row->STAGEID;
		}
		return $retResult;
	}
	
	private function getLastContactNext($clientId) {
		$retResult = '';
		$sql = "SELECT `NEXTCONTACT` FROM `treaty` WHERE `ID` IN "
				. "(SELECT MAX(`ID`) FROM `treaty` WHERE `CLIENTID` = '" . $clientId . "')";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		$row = $qresult->fetch_object();
		if ($row) {
			$retResult = $row->NEXTCONTACT;
		}
		return $retResult;
	}
	
	private function getLastComment($clientId) {
		$retResult = '';
		$sql = "SELECT `COMMENT` FROM `treaty` "
			. "WHERE `ID` IN "
			. "(SELECT MAX(`ID`) FROM `treaty` WHERE `CLIENTID` = '" . $clientId . "')";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		$row = $qresult->fetch_object();
		if ($row) {
			$retResult = $row->COMMENT;
		}
		return $retResult;
	}
	
	public function getMainClientList($sortByDate = false)
	{
		$retResult = array();
		/*
		$oneRow = array();
		$oneRow["CRDATE"] = '<b>Дата</b>';
		$oneRow["CLNAME"] = '<b>Имя клиента</b>';
		$oneRow["PHONENUM"] = '<b>Номер телефона</b>';
		$oneRow["SOURCENAME"] = '<b>Источник</b>';
		$oneRow["CLOCATION"] = '<b>Расположение</b>';
		$oneRow ["FULLNAME"] = '<b>Менеджер</b>';
		$oneRow ["STAGENAME"] = '<b>Стадия</b>';
		$oneRow ["NEXTCONTACT"] = '<a href="?action=main&sortby=nextcontact"><b>След. контакт</b></a>';
		$retResult[] = $oneRow;
		*/
		
		$sql = "SELECT `cl`.`CRDATE`,"
		."`cl`.`ID`,"
		."`cl`.`CLNAME`,"
		."`cl`.`PHONENUM`,"
		."`sr`.`SOURCENAME`,"
		."`cl`.`CLOCATION`,"
		."`mn`.`FULLNAME`"
		."FROM"
		."`clients` AS `cl`"
		."LEFT JOIN"
		."`sourceinfo` AS `sr` ON `cl`.`SOURCEID` = `sr`.`ID`"
		."LEFT JOIN"
		."`managers` AS `mn` ON `cl`.`MANAGERID` = `mn`.`ID`"
		."ORDER BY `cl`.`ID`";
		
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		while ($row = $qresult->fetch_object()) {
			$oneRow = array();
			$oneRow["CRDATE"] = $this->parseDateTime($row->CRDATE);	
			//$oneRow["CLNAME"] = HtmlOut::link($row->CLNAME, '?action=clientdetail&clientid=' . $row->ID);
			$oneRow["CLNAME"] = HtmlOut::evLink($row->CLNAME, '?action=clientdetail&clientid=' . $row->ID, $this->getLastComment($row->ID));
			$oneRow["PHONENUM"] = $row->PHONENUM;
			$oneRow["SOURCENAME"] = $row->SOURCENAME;
			$oneRow["CLOCATION"] = $row->CLOCATION;
			$oneRow ["FULLNAME"] = $row->FULLNAME;
			$oneRow ["STAGENAME"] = $this->getLastStageName($row->ID);
			$lastContactDate = $this->getLastContactNext($row->ID);
			$oneRow ["NEXTCONTACT"] = $this->parseDate($lastContactDate);
			
			// Код генерации ключа массива добавлен для возможности сортировки по $lastContactDate
			//$retResult[] = $oneRow;
			$stamp = $this->dateToStamp($lastContactDate);
			$newStamp = $this->getUniqueArrayKey($stamp, $retResult);
			//echo $newStamp . "<br>";
			$retResult[$newStamp] = $oneRow;
			if ($sortByDate == true) { ksort($retResult); }
		}
		$qresult->close();
		return $retResult;
	}
	
	public function getSourcesList() {
		$retResult = array();
		$sql = "SELECT * FROM `sourceinfo`";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		while ($row = $qresult->fetch_object()) {			
			$retResult[$row->ID] = $row->SOURCENAME;
		}
		return $retResult;
	}
	
	public function getManagerList() {
		$retResult = array();
		$sql = "SELECT `ID`, `FULLNAME` FROM `managers` WHERE `ACTIVE` = '1'";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		while ($row = $qresult->fetch_object()) {
			$retResult[$row->ID] = $row->FULLNAME;
		}
		return $retResult;
	}
	
	public function getServiceList() {
		$retResult = array();
		$sql = "SELECT * FROM `services`";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		while ($row = $qresult->fetch_object()) {
			$retResult[$row->ID] = $row->SNAME;
		}
		return $retResult;
	}
	
	public function getStageList() {
		$retResult = array();
		$sql = "SELECT `ID`, `STAGENAME` FROM `stage`";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		while ($row = $qresult->fetch_object()) {
			$retResult[$row->ID] = $row->STAGENAME;
		}
		return $retResult;
	}
	
	public function getManagerId($login) {
		$retResult = -1;
		$sql = "SELECT `ID` FROM `managers` WHERE `LOGIN` = '" . $login . "'";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		$row = $qresult->fetch_object();
		if ($row) { $retResult = $row->ID; }
		$qresult->close();
		return $retResult;
	}
	
	public function getManagerInfo($id) {
		$retResult = array('FULLNAME' => 'Unknown', 'DEPNAME' => 'Error', 'BILLETNAME' => 'Error');
		$sql = "SELECT `mn`.`FULLNAME`, `dp`.`DEPNAME`, `bl`.`BILLETNAME` "
			. "FROM `managers` AS `mn` "
			. "LEFT JOIN `department` AS `dp` ON `mn`.`DEPID` = `dp`.`ID` "
			. "LEFT JOIN `billets` AS `bl` ON `mn`.`BILLETID` = `bl`.`ID` "
			. "WHERE `mn`.`ID` = '" . $id . "' AND `ACTIVE` = '1'";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		$row = $qresult->fetch_object();
		if ($row) { 
			$retResult['FULLNAME'] = $row->FULLNAME;
			$retResult['DEPNAME'] = $row->DEPNAME;
			$retResult['BILLETNAME'] = $row->BILLETNAME;
		}
		$qresult->close();
		return $retResult;
	}
	
	public function getNewIdFromTable($tableName) {
		$sql = "SELECT MAX(ID) AS `ID` FROM `" . $tableName . "`";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		$row = $qresult->fetch_object();
		$lastId = $row->ID;
		$qresult->close();
		$newId = $lastId + 1;
		
		$sql = "INSERT INTO `" . $tableName . "`(`ID`) VALUES (" . $newId . ")";
		$this->dblink->query($sql);
		$this->checkSQLError($sql);
		return $newId;
	}
	
	public function saveClientInfo($id, $name, $location, $phonenum, $sourceid, $managerid) {
		$sql = "UPDATE `clients` SET `CRDATE` = NOW(), `CLNAME` = '" . $name . "', `CLOCATION` = '" . $location 
			. "', `PHONENUM` = '" . $phonenum . "', `SOURCEID` = '" . $sourceid 
			. "', `MANAGERID` = '" . $managerid . "' WHERE `clients`.`ID` = " . $id;
		$this->dblink->query($sql);
		$this->checkSQLError($sql);
	}
	
	public function getClientInfo($id) {
		$retResult = array();
		$sql = "SELECT * FROM `clients` WHERE `ID` = '" . $id . "'";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		$row = $qresult->fetch_object();
		if ($row) {
			$retResult['ID'] = $row->ID;
			$retResult['CRDATE'] = $this->parseDateTime($row->CRDATE);
			$retResult['CLNAME'] = $row->CLNAME;
			$retResult['CLOCATION'] = $row->CLOCATION;
			$retResult['PHONENUM'] = $row->PHONENUM;
			$retResult['SOURCEID'] = $row->SOURCEID;
			$retResult['MANAGERID'] = $row->MANAGERID;
		}
		$qresult->close();
		return $retResult;
	}
	
	private function clearClientServices($id) {
		$sql = "DELETE FROM `clientsavor` WHERE `CLIENTID` = '" . $id . "'";
		$this->dblink->query($sql);
		$this->checkSQLError($sql);
	}
	
	private function insertClientServicesList($id, $servIds) {
		foreach ($servIds as $servId) {
			$recId = $this->getNewIdFromTable('clientsavor');
			$sql = "UPDATE `clientsavor` SET `CLIENTID`='" . $id . "',`SERVICEID`='" . $servId . "' WHERE `ID` = '" . $recId . "'";
			$this->dblink->query($sql);
			$this->checkSQLError($sql);
		}
	}
	
	public function updateClientServices($post) {
		$servIdList = array();
		foreach ($post as $key => $value) {
			if (strpos($key, 'serv') !== false) {
				$servIdList[] = str_replace('serv', '', $key);
			}
		}
		
		$this->clearClientServices($post['clientid']);
		$this->insertClientServicesList($post['clientid'], $servIdList);
	}
	
	public function getClientServicesList($id) {
		$retResult = array();
		$sql = "SELECT `SERVICEID` FROM `clientsavor` WHERE `CLIENTID` = '" . $id . "'";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		while ($row = $qresult->fetch_object()) {
			$retResult[] = $row->SERVICEID;
		}
		return $retResult;
	}
	
	public function addTreaty($clientId, $managerId, $stageId, $commentText, $nextDate) {
		$recId = $this->getNewIdFromTable('treaty');
		$sql = "UPDATE `treaty` SET `CRDATE` = NOW(), `CLIENTID` = '" . $clientId . "', `MANAGERID` = '" . $managerId 
			. "', `STAGEID` = '" . $stageId . "', `COMMENT` = '" . $commentText 
			. "', `NEXTCONTACT` = '" . $nextDate . "' WHERE `treaty`.`ID` = " . $recId;
		$this->dblink->query($sql);
		$this->checkSQLError($sql);
	}
	
	public function getClientTreaty ($clientId) {
		$retResult = array();
		$sql = "SELECT `tr`.`CRDATE`, `mn`.`FULLNAME`, `st`.`STAGENAME`, `tr`.`COMMENT`, `tr`.`NEXTCONTACT` "
				. "FROM "
				. "`treaty` AS `tr` "
				. "LEFT JOIN "
				. "`managers` AS `mn` ON `tr`.`MANAGERID` = `mn`.`ID` "
				. "LEFT JOIN "
				. "`stage` AS `st` ON `tr`.`STAGEID` = `st`.`ID` "
				. "WHERE `tr`.`CLIENTID` = '" . $clientId . "' "
				. "ORDER BY `tr`.`ID` DESC";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		while ($row = $qresult->fetch_object()) {
			$oneRow = array();
			$oneRow['CRDATE'] = $this->parseDateTime($row->CRDATE);
			$oneRow['FULLNAME'] = $row->FULLNAME;
			$oneRow['STAGENAME'] = $row->STAGENAME;
			$oneRow['COMMENT'] = $row->COMMENT;
			$oneRow['NEXTCONTACT'] = $this->parseDate($row->NEXTCONTACT);
			$retResult[] = $oneRow;
		}
		$qresult->close();
		return $retResult;
	}
	
	public function dateToMySlq($indate) {
		$darray = explode('-', $indate);
		$day = $darray[0];
		$month = $darray[1];
		$year = $darray[2];
		return $year . '-' . $month . '-' . $day;
	}
	
	private function getClientIdList($managerId) {
		$retResult = array();
		$sql = "SELECT `ID` FROM `clients` WHERE `MANAGERID` = '" . $managerId . "'";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		while ($row = $qresult->fetch_object()) {
			$retResult[] = $row->ID;
		}
		return $retResult;
	}
	
	private function dateToStamp($inDate) { // yyyy-mm-dd
		$charray = explode('-', $inDate);
		if (count($charray) < 3) {
			return false;
		}
		$chyear = $charray[0];
		$chmonth = $charray[1];
		$chday = $charray[2];
		
		return mktime(0, 0, 0, $chmonth, $chday, $chyear);
	}
	
	private function getUniqueArrayKey($inKey, array $inArray) {
		$retResult = $inKey;
		reset($inArray);
		foreach ($inArray as $key => $value) {
			if ($retResult == $key) {
				$retResult++;
			}
		}
		return $retResult;
	}
	
	private function isToday($chdate) { // yyyy-mm-dd
		$retResult = false;
		date_default_timezone_set('Europe/Kiev');
		
		$charray = explode('-', $chdate);
		if (count($charray) < 3) {
			return false;
		}
		
		$chyear = $charray[0];
		$chmonth = $charray[1];
		$chday = $charray[2];
		
		$nday = date('j');
		$nmonth = date('n');
		$nyear = date('Y');
		
		$chtime = mktime(0, 0, 0, $chmonth, $chday, $chyear);
		$ntime = mktime(0, 0, 0, $nmonth, $nday, $nyear);
		
		if (($ntime - $chtime) == 0) { $retResult = true; }
		return $retResult;
	}
	
	private function isOverdue($chdate) { // yyyy-mm-dd
		$retResult = false;
		date_default_timezone_set('Europe/Kiev');
		
		$charray = explode('-', $chdate);
		if (count($charray) < 3) {
			return false;
		}
		$chyear = $charray[0];
		$chmonth = $charray[1];
		$chday = $charray[2];
		
		$nday = date('j');
		$nmonth = date('n');
		$nyear = date('Y');
		
		$chtime = mktime(0, 0, 0, $chmonth, $chday, $chyear);
		$ntime = mktime(0, 0, 0, $nmonth, $nday, $nyear);
		if ($ntime > $chtime) { $retResult = true; }
		return $retResult;
	}
	
	/*
	 * Взять список ид клиентов по менеджеру
	 * прокрутить и посчитать кол-во клиентов по сегодняшней дате
	 * по просрочке
	 * и по списку состояний
	 */
	
	public function getClientStatusesCount($managerId) {
		// Инициализация результирующего массива
		$retResult = array('TODAY' => 0, 'OVERDUE' => 0);
		$stageList = $this->getStageList();
		foreach ($stageList as $stageId => $stageName) {
			$retResult[$stageId] = 0;
		}

		// Обработка
		$clientList = $this->getClientIdList($managerId);
		foreach ($clientList as $clientId) {
			$lastContactNext = $this->getLastContactNext($clientId);
			if ($this->isToday($lastContactNext) == true) {
				$retResult['TODAY']++;
			}
			
			if ($this->isOverdue($lastContactNext) == true) {
				$retResult['OVERDUE']++;
			}
			
			$lastStageId = $this->getLastStageId($clientId);
			if ($lastStageId != -1) {
				$retResult[$lastStageId]++;
			}
		}
		return $retResult;
	}
	
	private function updateClientData($uId, $uDateTime, $uName, $uLocation, $uPhone, $uSourceId, $uManagerId) {
		$sql = "UPDATE `clients` SET `CRDATE` = '" . $uDateTime . "', `CLNAME` = '" . $uName . "', `CLOCATION` = '" 
				. $uLocation . "', `PHONENUM` = '" . $uPhone . "', `SOURCEID` = '" . $uSourceId 
				. "', `MANAGERID` = '" . $uManagerId . "' WHERE `clients`.`ID` = " . $uId;
		$this->dblink->query($sql);
		$this->checkSQLError($sql);
	}
	
	private function getManagerIdByName($name) {
		$retResult = '-1';
		$sql = "SELECT `ID` FROM `managers` WHERE `FULLNAME` LIKE '" . $name . "'";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		if ($qresult->num_rows > 0) {
			$row = $qresult->fetch_object();
			$retResult = $row->ID;
		}
		$qresult->close();
		return $retResult;
	}
	
	private function getLastManagerId() {
		$retResult = '-1';
		$sql = "SELECT MAX(`ID`) AS `LASTID` FROM `managers`";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		if ($qresult->num_rows > 0) {
			$row = $qresult->fetch_object();
			$retResult = $row->LASTID;
		}
		$qresult->close();
		return $retResult;
	}
	
	private function createManagerFromName($name, $depId, $defBilletId) {
		$newManagerId = $this->getLastManagerId() + 1;
		$sql = "INSERT INTO `managers` (`ID`, `CRDATE`, `FULLNAME`, `LOGIN`, `PASSWORD`, `LASTLOGIN`, `DEPID`, `BILLETID`, `ACTIVE`)" 
			." VALUES ('" . $newManagerId . "', NOW(), '" . $name . "', NULL, NULL, NULL, '" . $depId . "', '" . $defBilletId . "', '0')";
		$this->dblink->query($sql);
		$this->checkSQLError($sql);
		return $newManagerId;
	}
	
	private function getDepartamentIdByName($name) {
		$retResult = '-1';
		//$sql = "SELECT `ID` FROM `managers` WHERE `FULLNAME` LIKE '" . $name . "'";
		$sql = "SELECT `ID` FROM `department` WHERE `DEPNAME` LIKE '" . $name . "'";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		if ($qresult->num_rows > 0) {
			$row = $qresult->fetch_object();
			$retResult = $row->ID;
		}
		$qresult->close();
		return $retResult;
	}
	
	private function createDepartamentFromName($name) {
		$newDepId = $this->getNewIdFromTable('department');
		$sql = "UPDATE `department` SET `DEPNAME` = '" . $name . "' WHERE `department`.`ID` = " . $newDepId;
		$this->dblink->query($sql);
		$this->checkSQLError($sql);
		return $newDepId;
	}
	
	private function getSourceIdByName($name) {
		$retResult = '-1';
		$sql = "SELECT `ID` FROM `sourceinfo` WHERE `SOURCENAME` LIKE '" . $name . "'";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		if ($qresult->num_rows > 0) {
			$row = $qresult->fetch_object();
			$retResult = $row->ID;
		}
		$qresult->close();
		return $retResult;
	}
	
	private function createSourceByName($name) {
		$newSourceId = $this->getNewIdFromTable('sourceinfo');
		$sql = "UPDATE `sourceinfo` SET `SOURCENAME` = '" . $name . "' WHERE `sourceinfo`.`ID` = " . $newSourceId;
		$this->dblink->query($sql);
		$this->checkSQLError($sql);
		return $newSourceId;
	}
	
	private function getServiceIdByName($name) {
		$retResult = '-1';
		$sql = "SELECT `ID` FROM `services` WHERE `SNAME` LIKE '" . $name . "'";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		if ($qresult->num_rows > 0) {
			$row = $qresult->fetch_object();
			$retResult = $row->ID;
		}
		$qresult->close();
		return $retResult;
	}
	
	private function createServiceByName($name) {
		$newServiceId = $this->getNewIdFromTable('services');
		$sql = "UPDATE `services` SET `SNAME` = '" . $name . "' WHERE `services`.`ID` = " . $newServiceId;
		$this->dblink->query($sql);
		$this->checkSQLError($sql);
		return $newServiceId;
	}
	
	private function getStageIdByName($name) {
		$retResult = '-1';
		$sql = "SELECT `ID` FROM `stage` WHERE `STAGENAME` LIKE '" . $name . "'";
		$qresult = $this->dblink->query($sql);
		$this->checkSQLError($sql);
		if ($qresult->num_rows > 0) {
			$row = $qresult->fetch_object();
			$retResult = $row->ID;
		}
		$qresult->close();
		return $retResult;
	}
	
	private function createStageByName($name) {
		$newStageId = $this->getNewIdFromTable('stage');
		$sql = "UPDATE `stage` SET `STAGENAME` = '" . $name . "' WHERE `stage`.`ID` = " . $newStageId;
		$this->dblink->query($sql);
		$this->checkSQLError($sql);
		return $retResult;
	}
	
	// Метод импорта данных их "чистого текста", при отсутствии повторяющихся записей создается соответствующая новая
	public function importClientRawData($cDateTime,  
										$cName, 
										$cPhone, 
										$cService, 
										$cSource, 
										$cLocation, 
										$cManager, 
										$cDepartament, 
										$cComment, 
										$cLastDate, 
										$cState, 
										$cDenyComment){
		$depId = $this->getDepartamentIdByName($cDepartament);
		if ($depId == '-1') {
			$depId = $this->createDepartamentFromName($cDepartament);
		}
		
		$managerId = $this->getManagerIdByName($cManager);
		if ($managerId == '-1') {
			$managerId = $this->createManagerFromName($cManager, $depId, '0');
		}
		
		$sourceId = $this->getSourceIdByName($cSource);
		if ($sourceId == '-1') {
			$sourceId = $this->createSourceByName($cSource);
		}
		
		$newClientId = $this->getNewIdFromTable('clients');
		$this->updateClientData($newClientId, $cDateTime, $cName, $cLocation, $cPhone, $sourceId, $managerId);
		
		$serviceId = $this->getServiceIdByName($cService);
		if ($serviceId == '-1') {
			$serviceId = $this->createServiceByName($cService);
		}
		
		$this->insertClientServicesList($newClientId, array($serviceId));
		
		$stageId = $this->getStageIdByName($cState);
		if ($stageId == '-1') {
			$stageId = $this->createStageByName($cState);
		}
		
		$comment = $cComment;
		if (strlen($cDenyComment) > 0) {
			$comment = $cDenyComment;
		}
		
		$htmlComment = preg_replace('~[\\n]+?~', '<br />', $comment);
		$htmlComment = preg_replace('~[\\r]+?~', '', $comment);
		$this->addTreaty($newClientId, $managerId, $stageId, $htmlComment, $cLastDate);
	}
}
?>
